package com.example.davaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_random_number_layout.*

class RandomNumberActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_random_number_layout)
        init()
    }

    private fun init() {
        Button.setOnClickListener {
            Log.d("button", "Button is active!")
            showResult()
        }
    }

    private fun generateRandomNumber():Int{
        val randomNumber: Int = (-100..100).random()
        return randomNumber
    }

    private fun showResult(){
        val number: Int = generateRandomNumber()
        Log.d("randomNumber", "$number")
        if (number % 5 == 0){
            if (number / 5 > 0)
                randomNumberTextView.text ="Yes"
        }else{
            randomNumberTextView.text = "No"
        }

    }

}
